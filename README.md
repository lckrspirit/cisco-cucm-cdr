# Cisco CUCM - CDR

Created: Jan 29, 2021 11:28 AM

Состав:

`settings.py` - Файл с конфигурацией. Указываем место, куда будут прилетать cdr, и путь куда после будут перекладыватся. Также указываем настройки подключения к базе данных. 

`main.py` - Скрипт, который обрабатывает cdr-записи. 

---

Настройка:

- Нужно создать каталог, куда будут складываться cdr-записи.

    Например:

    - `/sftp/cucm/cdr`
    - `/sftp/cucm/cdr_temp`
- Создать пользователя, и группу. Добавить пользоватя в группу и сделать владельцем созданных каталогов.
- Установить и настроить mariadb. Создать базу данных и пользователя. Выдать пользователю права на базу.
- Перенести файлы парсера в рабочий каталог, сделать `main.py` исполняемым.
- Указать пути и настройки подключения к базе данных в `settings.py`.

    Например:

    ```sql
    # CDR Dir
    CDR_DIR = "/sftp/cucm/cdr"
    CDR_TMP = "/sftp/cucm/cdr_temp"

    # DataBase connect settings
    DATABASE = "Collect_CDR"
    DB_HOST = 'localhost'
    DB_PORT = 3306
    DB_USER = 'parser'
    DB_PASS = 'MyPassword'
    ```

- Создать cron-task, на запуск скрипта (В примере, скрипт запускается каждые 5 мин.):

```sql
# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed
*/5  *  * * *   root    /opt/cucm-scripts/main.py
```
