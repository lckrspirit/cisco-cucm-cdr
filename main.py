#!/bin/python3

import datetime 
import mariadb
import settings
import time
import sys
import os
import re


class CdrParser():
        def __init__(self, dir, columns):
                self.dir = dir
                self.columns = columns
                self.files = self.get_files(self.dir)
                self.title = ''
                self.db = DBwrite()
                self.date = datetime.date.today()


        def get_files(self, dir):
                list_dir = os.listdir(dir)
                for file in list_dir:
                        if os.path.isfile(dir+'/'+file):
                                pass
                        else:
                                list_dir.remove(file)
                return list_dir


        def parse_cdr(self, file):
                parse_list = []
                path = f'{self.dir}/{file}'
                with open(path, 'r+') as cdr:
                        for num, line  in enumerate(cdr.readlines()):
                                if num == 0:
                                        self.title = self.prepare_title(line)
                                elif num not in [0, 1]:
                                        record = self.prepare_body(line)
                                        udp_record = self.merge_record(self.title, record)
                                        one_record = self.filter_record(udp_record)
                                        parse_list.append(one_record)
                return parse_list


        def prepare_title(self, title):
                temp_lst = []
                for item in title.strip().split(','):
                        column = item.replace('\"', '')
                        temp_lst.append(column)
                return temp_lst


        def prepare_body(self, body):
                temp_list = []
                for item in body.strip().split(','):
                        if item == '\"\"':
                                temp_list.append('0')
                        else:
                                temp_list.append(item.replace('\"', ''))
                return temp_list


        def merge_record(self, title, record):
                return dict(zip(title, record))


        def filter_record(self, record):
                temp_record = {}
                for item in record.items():
                        if item[0] in self.columns:
                                temp_record[item[0]] = item[1]
                return temp_record


        def run(self):
                date = self.date.strftime('Records-%m-%Y')
                cdr_pattr = "cdr_"
                for file in self.files:
                        if re.match(cdr_pattr, file):
                                one_cdr_records = self.parse_cdr(file)
                                schemes = self.db.check_table(date)
                                self.db.db_insert(one_cdr_records, schemes)
                return True


        def cleaner(self):
                for file in self.files:
                        os.replace(f'{settings.CDR_DIR}/{file}', f'{settings.CDR_TMP}/{file}')


class DBwrite():
        def __init__(self):
                try:
                        self.con = mariadb.connect(
                                user = settings.DB_USER,
                                password = settings.DB_PASS,
                                host = settings.DB_HOST,
                                port = settings.DB_PORT,
                                database = settings.DATABASE,
                                autocommit = True
                        )
                except mariadb.Error as e:
                        print(f"Error connecting: {e}")
                        sys.exit()
                self.cur = self.con.cursor()


        def check_table(self, schemes):
                self.cur.execute('SHOW TABLES')
                tables = self.cur.fetchall()
                for table in tables:
                        if table[0] == schemes:
                                return schemes
                body = f"CREATE TABLE `{settings.DATABASE}`.`{schemes}`({settings.DB_COLUMNS});"
                self.cur.execute(body)
                return schemes


        def db_insert(self, cdr_file, schemes):
                for record in cdr_file:
                        req = self.db_prepare_request(record, schemes)
                        self.cur.execute(req)


        def db_prepare_request(self, record, schemes):
                IFS = ', '
                key_tmp = []
                val_tmp = []
                for key, item in record.items():
                        if key in settings.DATE_FORMAT:
                                item_ = self.prepare_format(item)
                                key_tmp.append(key)
                                val_tmp.append(f"\'{item_}\'")
                        else:
                                key_tmp.append(key)
                                val_tmp.append(f"\'{item}\'")
                return f"INSERT INTO `{settings.DATABASE}`.`{schemes}`({IFS.join(key_tmp)}) VALUES ({IFS.join(val_tmp)})"


        def prepare_format(self, item):
                return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(item)))


if __name__ == "__main__":
        parser = CdrParser(settings.CDR_DIR, settings.TITLE_COLUMNS)
        if parser.run():
                parser.cleaner()
