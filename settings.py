#!/bin/python

# CDR Dir
CDR_DIR = "/sftp/cucm/cdr"
CDR_TMP = "/sftp/cucm/cdr_temp"

# DataBase connect settings
DATABASE = "Collect_CDR"
DB_HOST = 'localhost'
DB_PORT = 3306
DB_USER = 'parser'
DB_PASS = 'MyPassword'

TITLE_COLUMNS = ['dateTimeOrigination', 'callingPartyNumber',  'callingPartyUnicodeLoginUserID', 'origCause_value', 
                                'originalCalledPartyNumber', 'finalCalledPartyNumber', 
                                'destCause_value', 'dateTimeConnect', 'dateTimeDisconnect',
                                'lastRedirectDn', 'origDeviceName', 'destDeviceName', 
                                'origIpv4v6Addr', 'destIpv4v6Addr', 'originalCalledPartyPattern',
                                'lastRedirectingPartyPattern', 'huntPilotPattern', 'duration']

DB_COLUMNS = f"""`dateTimeOrigination` DATETIME NULL,
                                `callingPartyNumber` VARCHAR(100) NULL,
                                `callingPartyUnicodeLoginUserID` VARCHAR(100) NULL,
                                `origCause_value` INT NOT NULL,
                                `originalCalledPartyNumber` VARCHAR(100) NULL,
                                `finalCalledPartyNumber` VARCHAR(100) NULL,
                                `destCause_value` INT NOT NULL,
                                `dateTimeConnect` DATETIME NULL,
                                `dateTimeDisconnect` DATETIME NULL,
                                `lastRedirectDn` VARCHAR(100) NULL,
                                `origDeviceName` VARCHAR(100) NULL,
                                `destDeviceName` VARCHAR(100) NULL,
                                `origIpv4v6Addr` VARCHAR(100) NULL,
                                `destIpv4v6Addr` VARCHAR(100) NULL,
                                `originalCalledPartyPattern` VARCHAR(100) NULL,
                                `lastRedirectingPartyPattern` VARCHAR(100) NULL,
                                `huntPilotPattern` VARCHAR(100) NULL,
                                `duration` VARCHAR(100) NULL"""

DATE_FORMAT = ['dateTimeOrigination', 'dateTimeConnect', 'dateTimeDisconnect']
